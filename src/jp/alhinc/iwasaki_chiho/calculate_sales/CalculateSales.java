package jp.alhinc.iwasaki_chiho.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{

	public static void main(String[] args){

		HashMap<String, String> branchName = new HashMap<String, String>();
		HashMap<String, Long> branchSales = new HashMap<String, Long>();
		HashMap<String, String> commodityName = new HashMap<String, String>();
		HashMap<String, Long> commoditySales = new HashMap<String, Long>();

		if(!input(args[0], "branch.lst", branchName, branchSales, "[0-9]{3}","支店")){
			return;
		}
		if(!input(args[0], "commodity.lst",commodityName, commoditySales, "^[0-9A-Za-z]{8}$","商品")){
				return;
		}
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++){
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}
		for(int i = 0; i < rcdFiles.size() -1; i++){
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
			}
		}
		for(int i = 0; i < rcdFiles.size(); i++){
			BufferedReader br = null;
			try{
				File salesFile = rcdFiles.get(i);
				FileReader fr = new FileReader(salesFile);
				br = new BufferedReader(fr);

				ArrayList<String> rcdData = new ArrayList<String>();
				String line;
				while ((line = br.readLine()) != null){
					rcdData.add(line);
				}
				if(rcdData.size() != 3){
					System.out.println(salesFile.getName() + "のフォーマットが不正です");
					return;
				}
				if(!branchName.containsKey(rcdData.get(0))){
					System.out.println(salesFile.getName() + "の支店コードが不正です");
					return;
				}
				if(!commodityName.containsKey(rcdData.get(1))){
					System.out.println(salesFile.getName() + "の商品コードが不正です");
					return;
				}
				if(!rcdData.get(2).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long fileSale = Long.parseLong(rcdData.get(2));
				long targetSales = branchSales.get(rcdData.get(0));
				long totalSummary = targetSales + fileSale;

				long commoditySalesBybranch =commoditySales.get(rcdData.get(1));
				long totalSalesByCommodity = commoditySalesBybranch + fileSale;

				if(totalSummary >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(totalSalesByCommodity >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(rcdData.get(0), totalSummary);
				commoditySales.put(rcdData.get(1),totalSalesByCommodity);

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally{
				if(br != null){
					try{
						br.close();
					}catch (IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;


					}
				}
			}
		}

		if(!output(args[0], "branch.out", branchName, branchSales)){
			return;
		}
		if(!output(args[0], "commodity.out", commodityName, commoditySales)){
			return;
		}
    }

	public static boolean input(String pass, String fileName, Map<String, String> names, Map<String, Long> sales, String codeNumbers, String error) {
		BufferedReader br = null;
		try{
			File file = new File(pass,fileName);
			if(!file.exists()){
				System.out.println(error + "定義ファイルが存在しません。");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null){
				String array[] = line.split(",");

				if((array.length != 2) || (!array[0].matches(codeNumbers))){
					System.out.println(error + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(array[0], array[1]);
				sales.put(array[0], (long) 0);
			}
			
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			if(br != null){
				try{
					br.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean output(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try{
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(Map.Entry<String, Long> entry : sales.entrySet()){
				bw.write(entry.getKey() + "," + names.get(entry.getKey()) + "," + entry.getValue());
				bw.newLine();
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}